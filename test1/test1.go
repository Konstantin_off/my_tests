// Дан каталог на диске. В нём находятся различные файлы.
// Часть файлов имеет имя, заданное шаблоном:

// image-[name]-[timestamp].tar.gz

// например:

// image-x64_600_sspace_windows-20180726T161105.tar.gz
// image-unix_1100_engine-e2k-linux_vdb-20180726T041855.tar.gz

// Необходимо написать функцию, которая найдет в каталоге все файлы, попадающие под шаблон, и вернет список, содержащий только [name] из названий файлов. Для приведенных выше примеров, список должен содержать элементы:
// x64_600_sspace_windows
// unix_1100_engine-e2k-linux_vdb

// Обратите внимание, что [name] может также содержать символы дефиса.
// Список должен содержать уникальные элементы.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
)

func main() {
	//получаем список файлов в папке
	files, err := ioutil.ReadDir("./test1_files")
	if err != nil {
		log.Fatal(err)
	}
	regTimestamp := regexp.MustCompile("-[0-9]{8}T[0-9]{6}\\.tar\\.gz$")
	regImage := regexp.MustCompile("^image-")

	for _, file := range files {

		s1 := regImage.FindStringIndex(file.Name())
		s2 := regTimestamp.FindStringIndex(file.Name())
		if len(s1) == 0 || len(s2) == 0 {
			continue
		}
		n, k := s1[1], s2[0]
		fmt.Println(n, "    ", k, "    ", file.Name()[n:k])
	}

}
