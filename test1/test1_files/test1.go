package main

import (
	"fmt"
	"strings"
)

func main() {
	b := "cheesee"
	if a := strings.Index(b, "ee"); a > 0 {
		fmt.Println(b[:a])
	}
	// files, err := ioutil.ReadDir("./test1_files")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// regTimestamp := regexp.MustCompile("-[0-9]{8}T[0-9]{6}\\.tar\\.gz$")
	// regImage := regexp.MustCompile("^image-")

	// for _, file := range files {

	// 	n, k := regImage.FindStringIndex(file.Name())[1], regTimestamp.FindStringIndex(file.Name())[0]
	// 	fmt.Println(file.Name()[n:k])
	// }

}
