package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"
)

var (
	result map[uint][]string

	lock sync.Mutex
)

func crawl(myUrl string, deep uint) {

	if deep == 0 {
		return
	}
	deep--

	//поиск url внутри страницы
	urls, err := findInsUrl(myUrl)
	if err != nil {
		log.Fatal(err)
	}
	if len(urls) == 0 {
		return
	}
	//вывод
	for _, u := range urls {
		//fmt.Println(u)

		result[deep+1] = append(result[deep+1], u)
		crawl(u, deep)
	}

}
func findInsUrl(myUrl string) ([]string, error) {
	//парсим Url чтобы получить хост
	u, err := url.Parse(myUrl)
	if err != nil {
		return nil, err
	}
	//log.Println(u.Host)
	host := strings.Replace(u.Host, ".", "\\.", -1)

	// получение страницы
	resp, err := http.Get(myUrl)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	//чтение кода страницы
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	text := string(b)

	//получение валидных ссылок
	re := regexp.MustCompile("<a href=\"(https?:\\/\\/" + host + "[\\/a-z,A-Z,-]*)\"")
	submatches := re.FindAllStringSubmatch(text, -1)

	var r []string
	for _, s := range submatches {
		if !contains(r, s[1]) {
			r = append(r, s[1])
		}
	}
	return r, nil
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func main() {
	t0 := time.Now()

	result = make(map[uint][]string)
	crawl("https://stackoverflow.com", 2)

	for i, urls := range result {
		fmt.Println(i)
		for _, u := range urls {
			fmt.Println(u)
		}
	}

	fmt.Printf("Elapsed time: %v", time.Since(t0))

}
