package db

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

type Connection struct {
	db        *sql.DB
	isConnect bool
}

//подключение к БД
func (c *Connection) Connect() {

	var err error
	c.db, err = sql.Open("mysql", "root:100496@tcp(localhost:3306)/crawlerdb?charset=utf8")
	if err != nil {
		log.Fatal(err)
	}
	//проверка подключения к БД и создание таблицы, если нет
	if c.db.Ping() != nil {
		err = c.createIfNotExist()
		if err != nil {
			log.Fatal(err)
		}
	}

	c.isConnect = true
}

//Отключение
func (c *Connection) Disconnect() {
	c.db.Close()
	c.isConnect = false
}

//создание БД если не создана
func (c *Connection) createIfNotExist() error {

	var err error
	c.db, err = sql.Open("mysql", "root:100496@tcp(localhost:3306)/?charset=utf8")
	if err != nil {
		return err
	}
	//создание базы данных если не существует
	_, err = c.db.Query("CREATE DATABASE IF NOT EXISTS crawlerdb")
	if err != nil {
		return err
	}
	c.db, err = sql.Open("mysql", "root:100496@tcp(localhost:3306)/crawlerdb?charset=utf8")
	if err != nil {
		return err
	}

	//создание таблицы если не существует
	_, err = c.db.Query("CREATE TABLE if not exists `urls` ( " +
		"`parentUrl` NVARCHAR(200) NOT NULL, `subUrls` NVARCHAR(8000) NULL, " +
		" PRIMARY KEY (`parentUrl`),  UNIQUE INDEX `parentUrl_UNIQUE` (`parentUrl` ASC) VISIBLE)" +
		"ENGINE = Innodb DEFAULT CHARACTER SET = utf8;")
	if err != nil {
		return err
	}
	return nil
}

//вставка url
func (c *Connection) InsertUrls(parentUrl string, subUrls []string) error {
	if !c.isConnect {
		c.Connect()
	}
	urls := strings.Join(subUrls, "; ")
	if _, err := c.db.Exec("insert into urls (parenturl,suburls) values (?,?)", parentUrl, urls); err != nil {
		return err
	}
	return nil
}

//Множественная вставка
func (c *Connection) MultipleInsertUrls(urlsMap map[string][]string) error {
	if !c.isConnect {
		c.Connect()
	}

	queryStr := "insert ignore into urls (parenturl,suburls) values"

	for k, v := range urlsMap {

		urls := strings.Join(v, "; ")
		queryStr += fmt.Sprintf(" (\"%v\", \"%v\"),", k, urls)
	}
	queryStr = queryStr[:len(queryStr)-1]

	if _, err := c.db.Exec(queryStr); err != nil {
		return err
	}
	return nil
}

//проверка есть ли url в БД
func (c *Connection) Contains(parentUrl string) bool {
	if !c.isConnect {
		c.Connect()
	}

	row := c.db.QueryRow("select count(*) from urls where parenturl = ?", parentUrl)
	var res int
	if err := row.Scan(&res); err != nil {
		log.Fatal(err)
	}

	if res == 1 {
		return true
	}
	return false
}
