// Задача:
// Разработать простенький краулер, которому на вход подаётся URL и глубина сбора.
// Нужно загрузить страницу по этому URL, распарсить её и вытянуть все ссылки.
// Те ссылки, что ведут на тот же домен, попадают на вход краулеру с уменьшенной глубиной сбора.
// Если глубина сбора ноль, то ссылка не загружается.
// Полученные страницы нужно сохранить в какую-то встраиваемую базу ключ-значение.
// Например, LevelDB. Ключом будет выступать url, а значением страница.

// В задаче необходимо показать умение пользоваться примитивами Go, писать идиоматичный код и декомпозировать задачу на более мелкие логические составляющие.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"./db"
)

var urlsMap map[string][]string

func crawl(myUrl string, deep uint, done *sync.WaitGroup) {

	if done != nil {
		defer done.Done()
	}
	if deep == 0 {
		return
	}

	//поиск всех url внутри страницы
	subUrls, err := findInsUrl(myUrl)
	if err != nil {
		log.Fatal(err)
	}
	if len(subUrls) == 0 {
		return
	}

	urlsMap[myUrl] = subUrls

	var d sync.WaitGroup
	for _, u := range subUrls {
		//fmt.Println(u)
		if len(urlsMap[u]) != 0 {
			continue
		}
		d.Add(1)
		go crawl(u, deep-1, &d)

	}
	//ожидание завершения
	d.Wait()

}

//возврат всех ссылок со страницы
func findInsUrl(myUrl string) ([]string, error) {
	//получение хоста
	u, err := url.Parse(myUrl)
	if err != nil {
		return nil, err
	}
	host := strings.Replace(u.Host, ".", "\\.", -1)

	// получение страницы
	resp, err := http.Get(myUrl)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	//чтение кода страницы
	p, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	page := string(p)

	//получение валидных ссылок
	re := regexp.MustCompile("<a [ ,a-z,=,\",A-Z,\\-,:,_,0-9]*href=\"((https?:\\/\\/(www\\.)?" + host + ")?[\\/a-z,A-Z,-]*)[?,=,a-z,0-9,A-Z,\\-,_,,&;]*\"")
	submatches := re.FindAllStringSubmatch(page, -1)

	var urls []string
	for _, s := range submatches {
		if !strings.Contains(s[1], u.Host) {
			s[1] = myUrl + s[1]
		}
		if !contains(urls, s[1]) {
			urls = append(urls, s[1])
		}
	}
	return urls, nil
}

//проверка содержания строки в срезе
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

var DB db.Connection

func main() {
	//запуск таймера
	t0 := time.Now()
	urlsMap = make(map[string][]string)
	//db.InsertUrls("1", []string{"a", "b", "c"})
	//fmt.Println(DB.Contains("ololo"))

	url := "https://google.ru"
	crawl(url, 2, nil)

	fmt.Println("___________________")

	DB.Connect()
	if err := DB.MultipleInsertUrls(urlsMap); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Elapsed time: %v", time.Since(t0))

}
