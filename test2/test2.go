// Реализовать функцию, которая запускает штатную linux-утилиту "whois" для переданного доменного имени, разбирает вывод и возвращает:
// - дату создания домена в виде той или иной структуры данных (например, https://docs.python.org/3/library/time.html#time.struct_time для Python)
// - список name server'ов (если получены)
// - название организации (если есть)

// Если произошла ошибка / дата не найдена - возвращать False.
// Проверять работу на:
// drweb.com
// drweb.ru
// drweb.net
// drweb.de

package main

import (
	"fmt"
	"strings"
	"time"

	"./cmd"
)

func main() {
	sites := []string{"drweb.com", "drweb.ru", "drweb.net", "drweb.de"}
	//sites := []string{"drweb.com"}
	for _, site := range sites {
		fmt.Println(site)
		whois, err := cmd.Whois(site)
		if err != nil {
			panic(err)
		}
		// keys := reflect.ValueOf(whois).MapKeys()

		// for _, key := range keys {
		// 	fmt.Print(key, ",")
		// }

		fmt.Println()
		if strings.Contains(site, ".ru") {
			if whois["created"] != "" {
				created, err := time.Parse(time.RFC3339, whois["created"])
				fmt.Println("Домен создан: ", created)
				if err != nil {
					fmt.Println("Домен создан: ", false)
				}
			} else {
				fmt.Println("Домен создан: ", false)
			}
			if whois["nserver"] != "" {
				fmt.Println("Список name server'ов: ", whois["nserver"])
			}
			if whois["org"] != "" {
				fmt.Println("Организация: ", whois["org"])
			}
		}
		if strings.Contains(site, ".com") {
			if whois["Creation Date"] != "" {

				if i := strings.Index(whois["Creation Date"], "; "); i > 0 {
					whois["Creation Date"] = whois["Creation Date"][:i]
				}
				created, err := time.Parse(time.RFC3339, whois["Creation Date"])
				fmt.Println("Домен создан: ", created)
				if err != nil {
					fmt.Println("Домен создан: ", false)
				}
			} else {
				fmt.Println("Домен создан: ", false)
			}

			if whois["Name Server"] != "" {
				fmt.Println("Список name server'ов: ", whois["Name Server"])
			}
		}
		if strings.Contains(site, ".net") {

			if whois["Creation Date"] != "" {
				if i := strings.Index(whois["Creation Date"], "; "); i > 0 {
					whois["Creation Date"] = whois["Creation Date"][:i]
				}
				created, err := time.Parse(time.RFC3339, whois["Creation Date"])
				fmt.Println("Домен создан: ", created)
				if err != nil {
					fmt.Println("Домен создан: ", false)
				}
			} else {
				fmt.Println("Домен создан: ", false)
			}
			if whois["Name Server"] != "" {
				fmt.Println("Список name server'ов: ", whois["Name Server"])
			}

			if whois["Registrant Organization"] != "" {
				fmt.Println("Организация: ", whois["Registrant Organization"])
			}
		}
		if strings.Contains(site, ".de") {
			if whois["Created"] != "" {
				created, err := time.Parse(time.RFC3339, whois["Created"])
				fmt.Println("Домен создан: ", created)
				if err != nil {
					fmt.Println("Домен создан: ", false)
				}
			} else {
				fmt.Println("Домен создан: ", false)
			}
			if whois["Nserver"] != "" {
				fmt.Println("Список name server'ов: ", whois["Nserver"])
			}
		}

		fmt.Println("**********")
	}

}
