package cmd

import (
	"os"
	"os/exec"
	"regexp"
	"strings"
)

func Whois(domen string) (map[string]string, error) {

	cmd := exec.Command("whois", domen)

	cmd.Stdin = os.Stdin

	out, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	str := string(out[:])
	spaces := regexp.MustCompile(" +")
	str = spaces.ReplaceAllString(str, " ")
	re := regexp.MustCompile("[-,a-z,A-Z, ]+: .+")

	information := re.FindAllString(str, -1)
	return mapper(information), nil
}

//маппинг результата
func mapper(input []string) map[string]string {
	information := make(map[string]string)
	for _, t := range input {
		inf := strings.Split(t, ": ")
		spaces := regexp.MustCompile("^ +")
		inf[0] = spaces.ReplaceAllString(inf[0], "")
		if information[inf[0]] != "" {
			if !strings.Contains(strings.ToLower(information[inf[0]]), strings.ToLower(inf[1])) {
				information[inf[0]] += "; " + inf[1]
			}
		} else {
			information[inf[0]] = inf[1]
		}
	}

	return information
}
