// Реализовать функцию скачивания web-страницы по URL, поиску на ней всех тэгов <script>,
// и вывода: либо тело тэга, либо значение src - в зависимости от содержания.

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
)

func download(url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}
	text := string(b)
	re := regexp.MustCompile("<script[^>]*>(.*)<\\/script>")
	scripts := re.FindAllString(text, -1)

	resrc := regexp.MustCompile("src=[\"']([^'\"]*)[\"']")
	for _, script := range scripts {
		if i := resrc.FindStringIndex(script); len(i) > 0 {
			fmt.Println(script[i[0]+5 : i[1]-1])
		} else {

			str := re.FindStringSubmatch(script)[1]
			fmt.Println(str)
		}
		fmt.Println()
	}

	return nil

}

func main() {
	download("http://drweb.ru")

}
